package th.co.ktb.next.archetype.model.response;

import th.co.ktb.next.common.base.BaseResponse;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
/*
 * This is a sample model for response of an API.
 * 1. The response model must implement the BaseResponse interface.
 * 2. This class serves as output of the respective API.
 * */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ResponseMessage extends BaseResponse {

    private String message;
}