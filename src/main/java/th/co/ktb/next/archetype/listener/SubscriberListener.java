package th.co.ktb.next.archetype.listener;

import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.stereotype.Component;
import th.co.ktb.next.archetype.model.request.RequestMessage;
import th.co.ktb.next.archetype.model.response.ResponseMessage;
import th.co.ktb.next.archetype.service.SubscriberBaseService;
import th.co.ktb.next.archetype.util.MessageStream;

@Component
@EnableBinding(MessageStream.class)
public class SubscriberListener {

    private SubscriberBaseService subscriberBaseService;

    public SubscriberListener(SubscriberBaseService subscriberBaseService) {
        this.subscriberBaseService = subscriberBaseService;
    }

    public ResponseMessage getMessage(RequestMessage requestMessage) {
        return subscriberBaseService.execute(requestMessage);
    }
}
