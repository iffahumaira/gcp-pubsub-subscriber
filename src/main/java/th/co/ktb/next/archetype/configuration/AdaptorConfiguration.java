package th.co.ktb.next.archetype.configuration;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.cloud.gcp.pubsub.core.PubSubTemplate;
import org.springframework.cloud.gcp.pubsub.core.publisher.PubSubPublisherTemplate;
import org.springframework.cloud.gcp.pubsub.core.subscriber.PubSubSubscriberTemplate;
import org.springframework.cloud.gcp.pubsub.support.converter.JacksonPubSubMessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AdaptorConfiguration {

    @Bean
    public JacksonPubSubMessageConverter jacksonPubSubMessageConverter(ObjectMapper objectMapper) {
        return new JacksonPubSubMessageConverter(objectMapper);
    }

    @Bean
    public PubSubTemplate pubSubTemplate(PubSubPublisherTemplate pubSubPublisherTemplate,
                                         PubSubSubscriberTemplate pubSubSubscriberTemplate) {
        return new PubSubTemplate(pubSubPublisherTemplate, pubSubSubscriberTemplate);
    }
}
