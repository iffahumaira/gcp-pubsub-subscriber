package th.co.ktb.next.archetype.service;

import com.google.auth.oauth2.ServiceAccountCredentials;
import com.google.cloud.MonitoredResource;
import com.google.cloud.logging.*;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cloud.gcp.pubsub.core.PubSubTemplate;
import org.springframework.cloud.gcp.pubsub.integration.AckMode;
import org.springframework.cloud.gcp.pubsub.integration.inbound.PubSubInboundChannelAdapter;
import org.springframework.cloud.gcp.pubsub.support.BasicAcknowledgeablePubsubMessage;
import org.springframework.cloud.gcp.pubsub.support.GcpPubSubHeaders;
import org.springframework.context.annotation.Bean;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.stereotype.Service;
import th.co.ktb.next.archetype.model.entity.Message;
import th.co.ktb.next.archetype.model.request.RequestMessage;
import th.co.ktb.next.archetype.model.response.ResponseMessage;
import th.co.ktb.next.common.service.BaseService;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Collections;

@Log4j2
@Service
public class SubscriberBaseService implements BaseService<RequestMessage, ResponseMessage> {

    @Bean
    public MessageChannel myInputChannel() {
        return new DirectChannel();
    }

    @Bean
    public PubSubInboundChannelAdapter messageChannelAdapter(@Qualifier("myInputChannel") MessageChannel inputChannel, PubSubTemplate pubSubTemplate) {

        PubSubInboundChannelAdapter adapter =
                new PubSubInboundChannelAdapter(pubSubTemplate, "next-dev-app");
        adapter.setOutputChannel(inputChannel);
        adapter.setAckMode(AckMode.MANUAL);
        adapter.setPayloadType(RequestMessage.class);

        return adapter;
    }

    @ServiceActivator(inputChannel = "myInputChannel")
    public void messageReceiver(@Header(GcpPubSubHeaders.ORIGINAL_MESSAGE) BasicAcknowledgeablePubsubMessage messageAck,
                                RequestMessage requestMessage) {
        log.info("Message arrived! Payload : " + requestMessage.toString());

        /* LOGGING TO STACKDRIVER */
/*        Logging logging = null;

        try {
            logging = LoggingOptions.newBuilder()
                    .setProjectId("mygcpcloudpubsub-258307")
                    .setCredentials(ServiceAccountCredentials.fromStream(new FileInputStream("src/main/resources/MyGCPCloudPubSub.json")))
                    .build().getService();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Message msg = new Message();
        msg.setMessage(requestMessage.getMessage());
        msg.setTopic(requestMessage.getTopic());

        String logName = "my-log";
        String text = "Hello World";

        LogEntry entry = LogEntry.newBuilder(Payload.JsonPayload(requestMessage))
                .setSeverity(Severity.ERROR)
                .setLogName(logName)
                .setResource(MonitoredResource.newBuilder("global").build())
                .build();

        logging.write(Collections.singleton(entry));

        System.out.println("Logged: {}}" + requestMessage.getMessage());

        log.info("TESTTTTTTTTTTTTTTT : {}", entry);*/
        /**/

        messageAck.ack();
    }

    public void executeCustom(@Header(GcpPubSubHeaders.ORIGINAL_MESSAGE) BasicAcknowledgeablePubsubMessage messageAck,
                              RequestMessage requestMessage) {
        messageAck.ack();
    }

    @Override
    public ResponseMessage execute(RequestMessage input) {
        return new ResponseMessage();
    }
}
