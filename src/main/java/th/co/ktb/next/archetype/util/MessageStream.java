package th.co.ktb.next.archetype.util;


import org.springframework.cloud.stream.annotation.Input;
import org.springframework.messaging.MessageChannel;

public interface MessageStream {

    String INPUT = "inbound";

    @Input(INPUT)
    MessageChannel subGcpInterceptor();
}
